package window;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class About extends JFrame {
	
	public About() {
		this.setTitle("About: Terminal");
		this.setSize(800, 480);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setupSkin();
		initUI();
		this.setVisible(true);
	}
	
	void initUI() {
		int style = 0;
		Font font = new Font(Font.MONOSPACED, style, 12);
		
		JPanel mainPanel = new JPanel();
		this.add(mainPanel);
	}
	
	private void setupSkin() {
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			
		} catch (ClassNotFoundException e) {
			
		} catch (InstantiationException e) {
			
		} catch (IllegalAccessException e) {
			
		}
	}
}
