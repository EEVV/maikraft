package window;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.swing.*;

public class Window extends JFrame {
	private JTextField txtTypeACommand;
	
	public Window() {
		this.setTitle("Terminal");
		this.setSize(800, 480);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setupSkin();
		initUI();
		this.setVisible(true);
	}
	
	void initUI() {
		int style = 0;
		Font font = new Font(Font.MONOSPACED, style, 12);
		
		JPanel mainPanel = new JPanel();
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnHelp = new JMenu("Help");
		
		JMenuItem mntmAbout = new JMenuItem("About");
		
		getContentPane().add(mainPanel);
		mainPanel.setLayout(null);
		
		txtTypeACommand = new JTextField();
		txtTypeACommand.setToolTipText("Execute a command");
		txtTypeACommand.setText("Type a command here!");
		txtTypeACommand.setBounds(10, 399, 663, 20);
		txtTypeACommand.setColumns(10);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 774, 377);
		panel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 0, 774, 377);
		
		TerminalPanel txtpnTerminal = new TerminalPanel();
		scrollPane.setViewportView(txtpnTerminal);
		txtpnTerminal.setText("Trolllll");
		txtpnTerminal.setEditable(false);
		txtpnTerminal.setFont(font);
		txtpnTerminal.setFocusable(true);
		
		JButton btnExecute = new JButton("Execute");
		btnExecute.setBounds(683, 398, 89, 23);
		txtpnTerminal.requestFocusInWindow();
		menuBar.add(mnHelp);
		mnHelp.add(mntmAbout);
		mainPanel.add(txtTypeACommand);
		mainPanel.add(panel);
		panel.add(scrollPane);
		btnExecute.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtTypeACommand.setText("");
			}
		});
		mainPanel.add(btnExecute);
	}
	
	private void setupSkin() {
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			
		} catch (ClassNotFoundException e) {
			
		} catch (InstantiationException e) {
			
		} catch (IllegalAccessException e) {
			
		}
	}
}
