package maikraft.input;

import org.lwjgl.input.Keyboard;

import maikraft.entity.Player;

public class InputHandler {
	public static void update(int delta) {
		if (Keyboard.isKeyDown(Keyboard.KEY_W)) {
			Player.move(0.0d, 0.0d, delta);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_A)) {
			Player.move(-1.0d*delta, 0.0d, 0.0d);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_S)) {
			Player.move(0.0d, 0.0d, 1.0d*delta);
		}
		if (Keyboard.isKeyDown(Keyboard.KEY_D)) {
			Player.move(1.0d*delta, 0.0d, 0.0d);
		}
	}
}
