package maikraft.main;

import maikraft.window.Window;
import maikraft.window.WindowSkin;

public class MainClass {
	public static void main(String[] args) {
		WindowSkin.setup();
		new Window();
	}
}
