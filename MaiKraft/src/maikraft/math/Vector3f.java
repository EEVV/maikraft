package maikraft.math;

public class Vector3f {
	private float x, y ,z;
	private float length;
	
	public Vector3f(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
		
		update();
	}
	public Vector3f() {
		this.x = 0.0f;
		this.y = 0.0f;
		this.z = 0.0f;
		
		this.length = 0.0f;
	}
	
	private void update() {
		length = (float) (Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
	}
	
	public float getX() {
		return this.x;
	}
	public float getY() {
		return this.y;
	}
	public float getZ() {
		return this.z;
	}
	
	public void setX(float x) {
		this.x = x;
		update();
	}
	public void setY(float y) {
		this.y = y;
		update();
	}
	public void setZ(float Z) {
		this.z = z;
		update();
	}
}
