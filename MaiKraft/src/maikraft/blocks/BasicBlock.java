package maikraft.blocks;

import maikraft.renderer.Renderer;

public class BasicBlock {
	private int pos[] = {0, 0, 0};
	
	public BasicBlock(int x, int y, int z) {
		pos[0] = x;
		pos[1] = y;
		pos[2] = z;
	}
	
	public void render() {
		Renderer.renderCube(pos[0], pos[1], pos[2], 0.5f, 0.5f, 0.5f);
	}
}
