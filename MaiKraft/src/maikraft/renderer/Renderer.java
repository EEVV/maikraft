package maikraft.renderer;

import org.lwjgl.opengl.GL11;

import maikraft.camera.Camera;

public class Renderer {
	private static float xc = Camera.getX();
	private static float yc = Camera.getY();
	private static float zc = Camera.getZ();
	
	public static void renderCube(float x, float y, float z, float xs, float ys, float zs) {
		xc = Camera.getX();
		yc = Camera.getY();
		zc = Camera.getZ();
		GL11.glBegin(GL11.GL_QUADS);
			//Front
			GL11.glVertex3f(-xs+x-xc, -ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, -ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, ys+y-yc, zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, ys+y-yc, zs+z-zs);
			//Back
			GL11.glVertex3f(-xs+x-xc, -ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(xs+x-xc, -ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(xs+x-xc, ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, ys+y-yc, -zs+z-zs);
			//Top
			GL11.glVertex3f(-xs+x-xc, ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, ys+y-yc, -zs+z-zs);
			//Bottom
			GL11.glVertex3f(-xs+x-xc, -ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, -ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, -ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, -ys+y-yc, -zs+z-zs);
			//Left
			GL11.glVertex3f(-xs+x-xc, -ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, -ys+y-yc, zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, ys+y-yc, zs+z-zs);
			GL11.glVertex3f(-xs+x-xc, ys+y-yc, -zs+z-zs);
			//Right
			GL11.glVertex3f(xs+x-xc, -ys+y-yc, -zs+z-zs);
			GL11.glVertex3f(xs+x-xc, -ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc, ys+y-yc, zs+z-zs);
			GL11.glVertex3f(xs+x-xc-xc, ys+y-yc, -zs+z-zs);
		GL11.glEnd();
	}
}
