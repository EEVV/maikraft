package maikraft.window;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class WindowSkin {
	public static void setup() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			
		} catch (ClassNotFoundException e) {
			
		} catch (InstantiationException e) {
			
		} catch (IllegalAccessException e) {
			
		}
	}
}
