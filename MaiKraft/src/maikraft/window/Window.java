package maikraft.window;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Window extends JFrame {
	public Window() {
		this.setTitle("MaiKraft");
		this.setSize(800, 400);
		this.setVisible(true);
		initUI();
	}
	
	void initUI() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(null);

		JButton btnPlay = new JButton("Play");
		btnPlay.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selfDestruct();
				new GameWindow("1280x720").run();
			}
		});
		btnPlay.setBounds(312, 258, 234, 71);
		mainPanel.add(btnPlay);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnAbout = new JMenu("About");
		menuBar.add(mnAbout);
		
		JMenuItem mntmHelp = new JMenuItem("Help");
		mnAbout.add(mntmHelp);
		
		
		
		getContentPane().add(mainPanel);
	}
	
	public void selfDestruct() {
		this.setVisible(false);
		this.dispose();
	}
}
