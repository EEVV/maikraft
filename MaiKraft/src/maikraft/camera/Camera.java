package maikraft.camera;

public class Camera {
	private static float[] position = {0.0f, 0.0f, 0.0f};
	private static float[] rotation = {15.0f, 0.0f, 0.0f};
	
	public static void translate(float x, float y, float z) {
		position[0] += x;
		position[1] += y;
		position[2] += z;
	}
	public static void setPos(float x, float y, float z) {
		position[0] += x;
		position[1] += y;
		position[2] += z;
	}
	public static void rotated(float x, float y) {
		rotation[0] += x;
		rotation[1] += y;
	}
	public static void setRot(float x, float y) {
		rotation[0] = x;
		rotation[1] = y;
	}
	
	public static float getX() {
		return position[0];
	}
	public static float getY() {
		return position[1];
	}
	public static float getZ() {
		return position[2];
	}
	
	public static float getRx() {
		return rotation[0];
	}
	public static float getRy() {
		return rotation[1];
	}
	public static float getRz() {
		return rotation[2];
	}
}
