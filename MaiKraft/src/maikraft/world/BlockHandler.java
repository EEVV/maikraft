package maikraft.world;

import org.lwjgl.opengl.GL11;

import maikraft.blocks.BasicBlock;
import maikraft.camera.Camera;

public class BlockHandler {
	private static BasicBlock[] block = new BasicBlock[32767];
	
	private static float crx = Camera.getRx();
	private static float cry = Camera.getRy();
	private static float crz = Camera.getRz();
	
	public static void add(short x, short y) {
		
	}
	
	public static void init() {
		block[0] = new BasicBlock(0, 0, -2);
		block[1] = new BasicBlock(1, 0, -2);
	}
	
	public static void render() {
		for (short i = 0; i<block.length; i++) {
			if (block[i] != null) {
				GL11.glColor3f(i/2.0f, i/2.0f, i/2.0f);
				GL11.glPushMatrix();
				GL11.glRotatef(crx, 1.0f, 0.0f, 0.0f);
				GL11.glRotatef(cry, 0.0f, 1.0f, 0.0f);
				GL11.glRotatef(crz, 0.0f, 0.0f, 1.0f);
				//GL11.glTranslatef(0.0f, 0.0f, -2.0f);
				//GL11.glRotatef(1.0f, crx, cry, crz);
				block[i].render();
				GL11.glPopMatrix();
			}
		}
	}
}
