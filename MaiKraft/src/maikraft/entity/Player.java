package maikraft.entity;

import maikraft.camera.Camera;

public class Player {
	private static double[] pos = {0.0f, 0.0f, 0.0f};
	private static double[] velocity = {0.0f, 0.0f, 0.0f};
	
	public static double getX() {
		return pos[0];
	}
	public static double getY() {
		return pos[1];
	}
	public static double getZ() {
		return pos[2];
	}
	
	public static void move(double x, double y, double z) {
		pos[0] += x;
		pos[1] += y;
		pos[2] += z;
		
		Camera.setPos((float) x, (float) y+1.75f, (float) z);
	}
	public static void jump(double strength) {
		velocity[2] += strength/9.8d;
	}
}
