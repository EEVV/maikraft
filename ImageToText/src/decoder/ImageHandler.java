package decoder;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class ImageHandler {
	
	private String path = null;
	private BufferedImage image = null;
	
	private int[][][] rawImage;
	private String decoded = "";
	
	public void setPath(String newPath) {
		path = newPath;
	}
	public String decodeToString() {
		String decoded = "";
		for (int x = 0; x<rawImage[x].length; x++) {
			for (int y = 0; y<rawImage[x][y].length; y++) {
				for (byte i = 0; i<4; i++) {
					decoded += (char) rawImage[x][y][i];
				}
			}
		}
		
		return decoded;
	}
	public void decodeChunk(int x, int y) {
		for (byte i = 0; i<4; i++) {
			decoded += (char) rawImage[x][y][i];
		}
	}
	public String getDecoded() {
		return decoded;
	}
	
	public int update() {
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			return -1;
		}
		rawImage = new int[image.getWidth()][image.getHeight()][4];
		for (int x = 0; x<image.getWidth(); x++) {
			for (int y = 0; y<image.getHeight(); y++) {
				
				Color cColor = new Color(image.getRGB(x, y));
				
				rawImage[x][y][0] = cColor.getRed();
				rawImage[x][y][1] = cColor.getGreen();
				rawImage[x][y][2] = cColor.getBlue();
				rawImage[x][y][3] = cColor.getAlpha();
			}
		}
		
		return 0;
	}
	
	public void encode(String text) {
		image = new BufferedImage(1, (int) (Math.ceil(text.length()/4.0d)), BufferedImage.TYPE_INT_ARGB);
		
		for (int i = 0; i<text.length(); i++) {
			int r = 0;
			int g = 0;
			int b = 0;
			int a = 0;
			
			r = (int) text.charAt(i);
			if (text.length() > i+1)
				g = (int) text.charAt(i+1);
			if (text.length() > i+2)
				b = (int) text.charAt(i+2);
			if (text.length() > i+3)
				a = (int) text.charAt(i+3);
			
			int col = (a << 24) | (r << 16) | (g << 8) | (b);
			image.setRGB(0, (int) (Math.floor(i/4.0d)), col);
		}
		
		try {
			ImageIO.write(image, "png", new File(path));
		} catch (IOException e) {
			System.out.println("Erro has uucuars");
			System.exit(-1);
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	
	public void encodeChunk(String text, int i) {
		image = new BufferedImage(1, (int) (Math.ceil(text.length()/4.0d)), BufferedImage.TYPE_INT_ARGB);
		
		int r = 0;
		int g = 0;
		int b = 0;
		int a = 0;
		
		r = (int) text.charAt(i);
		if (text.length() > i+1)
			g = (int) text.charAt(i+1);
		if (text.length() > i+2)
			b = (int) text.charAt(i+2);
		if (text.length() > i+3)
			a = (int) text.charAt(i+3);
		
		int col = (a << 24) | (r << 16) | (g << 8) | (b);
		image.setRGB(0, (int) (Math.floor(i/4.0d)), col);
	}
}
	/*
	private String path = null;
	private BufferedImage image = null;
	
	private int[][][] rawImage;
	
	public void setPath(String newPath) {
		path = newPath;
		//update();
	}
	public String decodeToString() {
		
		String decoded = "";
		
		for (int x = 0; x<rawImage.length; x++) {
			for (int y = 0; y<rawImage[x].length; y++) {
				decoded += (char) rawImage[x][y][0];
				decoded += (char) rawImage[x][y][1];
				decoded += (char) rawImage[x][y][2];
				decoded += (char) rawImage[x][y][3];
			}
		}
		
		return decoded;
	}
	
	public void update() {
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			System.out.println("\n(ERROR)");
			System.out.println("("+this+") Tried to load: "+path);
			System.out.println("Path is invalid");
			System.exit(-1);
		}
		
		System.out.println("Image size is: "+image.getWidth()+"x"+image.getHeight());
		System.out.println("Total pixels: "+image.getWidth()*image.getHeight()+"\n");
		rawImage = new int[image.getWidth()][image.getHeight()][4];
		
		System.out.println("Converting image to rawData");
		long startTime = System.nanoTime();
		for (int x = 0; x<image.getWidth(); x++) {
			for (int y = 0; y<image.getHeight(); y++) {
				
				Color currentColor = new Color(image.getRGB(x, y), true);
				
				int r = currentColor.getRed();
				int g = currentColor.getGreen();
				int b = currentColor.getBlue();
				int a = currentColor.getAlpha();
				
				rawImage[x][y][0] = r;
				rawImage[x][y][1] = g;
				rawImage[x][y][2] = b;
				rawImage[x][y][3] = a;
			}
		}
		long elapsedTime = System.nanoTime() - startTime;
		double elapsedSeconds = elapsedTime/1000000;
		System.out.println("Image to rawData successful, done in "+elapsedSeconds+"ms");
	}
	public void encode(String text) {		
		long startTime = System.nanoTime();
		System.out.println("Began encoding text...");
		char[] textArray = new char[text.length()];
		
		for (int i = 0; i<text.length(); i++) {
			textArray[i] = text.charAt(i);
		}
		
		BufferedImage decodedImage = new BufferedImage(1, (int) (Math.ceil(textArray.length/4.0d)), BufferedImage.TYPE_INT_ARGB);
		
		for (int i = 0; i<textArray.length; i+=4) {
			int r = 0;
			int g = 0;
			int b = 0;
			int a = 0;
			
			
			r = (int) textArray[i];
			if (textArray.length > i+1)
				g = (int) textArray[i+1];
			if (textArray.length > i+2)
				b = (int) textArray[i+2];
			if (textArray.length > i+3)
				a = (int) textArray[i+3];
			
			int col = (a << 24) | (r << 16) | (g << 8) | (b);
			decodedImage.setRGB(0, (int) (Math.floor(i/4.0d)), col);
		}
		
		long elapsedTime = System.nanoTime() - startTime;
		double elapsedSeconds = elapsedTime/1000000;
		System.out.println("Encoding text successful, done in "+elapsedSeconds+"ms");
		
		File outputFile = new File(path);
		try {
			ImageIO.write(decodedImage, "png", outputFile);
		} catch (IOException e) {
			System.out.println("Erro has uucuars");
			System.exit(-1);
			// TODO Auto-generated catch block
			//e.printStackTrace();
		}
	}
	*/
