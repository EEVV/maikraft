package decoder;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageHandler2 {
	private String path = null;
	private BufferedImage image = null;
	
	public void setPath(String newPath) {
		path = newPath;
	}
	public void update() {
		try {
			image = ImageIO.read(new File(path));
		} catch (IOException e) {
			System.out.println("(Error)"+this+" Tried to access an invalid path");
			//e.printStackTrace();
		}
	}
	public String decodeToString() {
		String returnString = "";
		
		Color xpixel = new Color(image.getRGB(0, 0));
		Color ypixel = new Color(image.getRGB(1, 0));
		
		int xpoint = 255*xpixel.getRed()+xpixel.getGreen();
		int xend = xpoint+xpixel.getBlue();
		int ypoint = 255*ypixel.getRed()+ypixel.getGreen();
		int yend = ypixel.getBlue();
		
		int[] xcolors = new int[3];
		int[] ycolors = new int[3];
		
		for (int x = xpoint; x<xend; x++) {
			for (int y = ypoint; y<yend; y++) {
				Color currentColor = new Color(image.getRGB(x, y));
				
				returnString += (char) currentColor.getRed();
				returnString += (char) currentColor.getGreen();
				returnString += (char) currentColor.getBlue();
			}
		}
		
		return returnString;
	}
}
