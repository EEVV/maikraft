package window;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class ImagePanel extends JPanel {
	
	private BufferedImage image;
	
	public ImagePanel() {
		try {
			image = ImageIO.read(new File("C:\\Users\\0eevv\\Desktop\\encodd.png"));
		} catch (IOException e) {
			System.out.println("Error loading preview image!");
			System.exit(-1);
		}
	}
	
	public void setImage(BufferedImage newImage) {
		image = newImage;
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 0, 0, 140, 140, null, null);
	}
}
