package window;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import decoder.*;

public class Window extends JFrame {
	
	private ImageHandler currentImage = new ImageHandler();
	
	private BufferedImage imagePreview;
	
	private JTextField textField;
	public Window() {
		
		this.setSize(400, 300);
		this.setTitle("TextToImage");
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		initUI();
		
		this.setVisible(true);
	}
	
	private void initUI() {
		setupSkin();
		
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(null);
		
		JLabel pathLabel = new JLabel("Path");
		pathLabel.setBounds(84, 9, 22, 14);
		//pathLabel.setToolTipText("");
		mainPanel.add(pathLabel);
		
		JTextField pathText = new JTextField(15);
		pathText.setBounds(111, 6, 126, 20);
		pathText.setToolTipText("Write the path to your image file");
		mainPanel.add(pathText);
		
		ImagePanel previewPanel = new ImagePanel();
		previewPanel.setBounds(111, 37, 273, 141);
		previewPanel.setImage(null);
		mainPanel.add(previewPanel);
		
		JButton pathUpdate = new JButton("Update");
		pathUpdate.setBounds(242, 5, 67, 23);
		pathUpdate.setToolTipText("Update the path, and check if it exists");
		pathUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					previewPanel.setImage(ImageIO.read(new File(pathText.getText())));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				currentImage.setPath(pathText.getText());
				previewPanel.repaint();
			}
		});
		mainPanel.add(pathUpdate);
		
		getContentPane().add(mainPanel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(111, 189, 273, 57);
		mainPanel.add(scrollPane);
		
		JTextArea textArea = new JTextArea();
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
		
		JButton btnRead = new JButton("Read");
		btnRead.setBounds(10, 223, 89, 23);
		btnRead.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentImage.update();
				textArea.setText(currentImage.decodeToString());
			}
		});
		mainPanel.add(btnRead);
		
		JProgressBar progressBar = new JProgressBar();
		getContentPane().add(progressBar, BorderLayout.SOUTH);
		//progressBar.setValue(50);
		
		JButton btnWrite = new JButton("Write");
		btnWrite.setBounds(10, 189, 89, 23);
		btnWrite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				writeToFile();
			}
		});
		mainPanel.add(btnWrite);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Type A", "Type B"}));
		comboBox.setMaximumRowCount(2);
		comboBox.setBounds(10, 6, 64, 20);
		mainPanel.add(comboBox);

		


	}
	private void writeToFile() {
		
	}
	private void setupSkin() {
		try {
			UIManager.setLookAndFeel(
					UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			
		} catch (ClassNotFoundException e) {
			
		} catch (InstantiationException e) {
			
		} catch (IllegalAccessException e) {
			
		}
	}
}
