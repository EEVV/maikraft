package lpr.Input;

import org.lwjgl.input.Mouse;

import lpr.voxels.ObjectHandler;

public class InputHandler {
	
	public static void update(int width, int height) {
		if (Mouse.isButtonDown(0)) {
			ObjectHandler.setX(Mouse.getX()-width/2.0f);
			ObjectHandler.setY(-(Mouse.getY()-height/2.0f));
		}
	}
}
