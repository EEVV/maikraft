package lpr.guiWindows;

import javax.swing.*;
import java.awt.Canvas;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import lpr.editorCanvas.EditorCanvas;
import lpr.gameWindow.GameWindow;

public class MainWindow extends JFrame implements Runnable {
	// Eclipse wants it so, k.
	private static final long serialVersionUID = 1L;
	private JTextField txtx;
	public static Canvas canvas;

	public MainWindow() {
		initUI();
		this.setVisible(true);
		this.setTitle("LPR");
		this.setSize(400, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void run() {
	}
	
	private void initUI() {
		SkinSetup.setup();
		getContentPane().setLayout(null);
		
		JPanel mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 384, 561);
		mainPanel.setLayout(null);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmOpenVxlEditor = new JMenuItem("Open vxl editor");
		mntmOpenVxlEditor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//selfDestruct();
				new VoxelEditorWindow();
				new Thread(new EditorCanvas()).start();
			}
		});
		
		mnFile.add(mntmOpenVxlEditor);
		JLabel title = new JLabel();
		title.setBounds(10, 10, 262, 21);
		title.setText("LPR - Best name evur!");
		mainPanel.add(title);
		
		getContentPane().add(mainPanel);
		
		JLabel lblResolution = new JLabel("Resolution:");
		lblResolution.setBounds(10, 42, 71, 14);
		mainPanel.add(lblResolution);
		
		txtx = new JTextField();
		txtx.setText("1280x720");
		txtx.setBounds(10, 55, 364, 20);
		mainPanel.add(txtx);
		txtx.setColumns(10);
		
		canvas = new Canvas();
		canvas.setBounds(10, 81, 364, 416);
		mainPanel.add(canvas);
		
		JButton btnLaunch = new JButton("Launch");
		btnLaunch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				selfDestruct();
				new GameWindow(txtx.getText()).run();
			}
		});
		btnLaunch.setBounds(285, 503, 89, 23);
		mainPanel.add(btnLaunch);
	}
	
	private void selfDestruct() {
		this.setVisible(false);
		this.dispose();
	}
	
	public static void main(String[] args) {
		new MainWindow();
	}
}