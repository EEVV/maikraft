package lpr.guiWindows;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class SkinSetup {
	public static void setup() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			
		} catch (ClassNotFoundException e) {
			
		} catch (InstantiationException e) {
			
		} catch (IllegalAccessException e) {
			
		}
	}
}
