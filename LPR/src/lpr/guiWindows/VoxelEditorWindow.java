package lpr.guiWindows;

import javax.swing.*;
import java.awt.Canvas;
import java.awt.Label;
import java.awt.BorderLayout;

public class VoxelEditorWindow extends JFrame {
	
	public static Canvas canvas;
	
	public VoxelEditorWindow() {
		initUI();
		this.setVisible(true);
		this.setTitle("Voxel Editor (vxl)");
		this.setLocationRelativeTo(null);
		this.setSize(600, 600);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
	public void initUI() {
		JPanel mainPanel = new JPanel();
		
		getContentPane().add(mainPanel);
		mainPanel.setLayout(null);
		
		canvas = new Canvas();
		canvas.setBounds(62, 70, 512, 481);
		mainPanel.add(canvas);
		
		SpinnerNumberModel spinnermodel = new SpinnerNumberModel(0.0f, 0.0f, 255.0f, 1.0f);
		JSpinner bitRedSpinner = new JSpinner(spinnermodel);
		bitRedSpinner.setToolTipText("Red color value");
		bitRedSpinner.setBounds(10, 36, 56, 20);
		mainPanel.add(bitRedSpinner);
		
		getContentPane().add(mainPanel);
		
		JLabel lblColor = new JLabel("Color (R, G, B, A) (HEX)");
		lblColor.setBounds(10, 11, 188, 14);
		mainPanel.add(lblColor);
		
		JSpinner bitGreenSpinner = new JSpinner(spinnermodel);
		bitGreenSpinner.setToolTipText("Green color value");
		bitGreenSpinner.setBounds(76, 36, 56, 20);
		mainPanel.add(bitGreenSpinner);
		
		JSpinner bitBlueSpinner = new JSpinner(spinnermodel);
		bitBlueSpinner.setToolTipText("Blue color value");
		bitBlueSpinner.setBounds(142, 36, 56, 20);
		mainPanel.add(bitBlueSpinner);
		
		JSpinner bitAlphaSpinner = new JSpinner(spinnermodel);
		bitAlphaSpinner.setToolTipText("Alpha color value");
		bitAlphaSpinner.setBounds(208, 36, 56, 20);
		mainPanel.add(bitAlphaSpinner);
	}
}
