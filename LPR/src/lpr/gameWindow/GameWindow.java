package lpr.gameWindow;

import java.awt.Canvas;

import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.*;

import lpr.Input.InputHandler;
import lpr.guiWindows.MainWindow;
import lpr.voxels.ObjectHandler;
import lpr.voxels.VoxelFile;

public class GameWindow {		
	
	long lastFrame;
	int fps;
	long lastFPS;
	boolean inited = false;
	
	private int width;
	private int height;
	
	public GameWindow(String resolution) {
		String[] newRes = resolution.split("x");
		
		setWidth(Integer.parseInt(newRes[0]));
		setHeight(Integer.parseInt(newRes[1]));
	}
	
	private void setWidth(int width) {
		this.width = width;
	}
	private void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}
	
	public void run() {
		try {
			Display.setDisplayMode(new DisplayMode(width, height));
			//Display.setParent(MainWindow.canvas);
			Display.setResizable(true);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		// Initializing opengl...
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		//GLU.gluPerspective(70, 1.8f, 0.5f, 100.0f);
		GLU.gluPerspective(70, ((float) Display.getWidth() / (float) Display.getHeight()), 0.5f, 100.0f);
		System.out.println("Java thinks that "+width+" / "+height+" is: "+((float)width/(float)height));
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		//GL11.glViewport(0, 0, WIDTH, HEIGHT);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClearColor(0.25f, 0.5f, 1.0f, 1.0f);
		
		getDelta();
		lastFPS = getTime();
		
		while (!Display.isCloseRequested()) {
			if (!inited) {
				init();
				inited = true;
			}
			int delta = getDelta();
			update(delta);
			
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
			//Background.render();
			//VoxelHandler.render();
			ObjectHandler.render();
			
			Display.update();
		}
		Display.destroy();
	}
	
	public void update(int delta) {
		InputHandler.update(width, height);
		updateFPS();
	}
	public void init() {
		ObjectHandler.init();
	}
	
	public int getDelta() {
		long time = getTime();
		int delta = (int) (time - lastFrame);
		lastFrame = time;
		
		return delta;
	}
	
	public long getTime() {
		return (Sys.getTime() * 1000) / Sys.getTimerResolution();
	}
	
	public void updateFPS() {
		if (getTime() - lastFPS > 1000) {
			Display.setTitle("LPR FPS: "+fps);
			fps = 0;
			lastFPS += 1000;
		}
		fps++;
	}
}
