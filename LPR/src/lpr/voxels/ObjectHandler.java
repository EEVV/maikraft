package lpr.voxels;

import org.lwjgl.opengl.GL11;

import lpr.renderer.Renderer;

public class ObjectHandler {
	private static VoxelObject[] objects = new VoxelObject[32767];
	
	private static float[] rotation = {0.0f, 0.0f};
	
	public static void setX(float newX) {
		rotation[0] = newX;
	}
	public static void setY(float newY) {
		rotation[1] = newY;
	}
	
	public static void init() {
		//objects[0] = new VoxelObject("assets\\models\\desk.vxl");
		for (short i = 0; i<100; i++) {
			objects[i] = new VoxelObject("assets\\models\\desk.vxl");
			objects[i].transform(i*2.0f, 1.0f, 1.0f);
		}
	}
	
	public static void render() {
		
		GL11.glPushMatrix();
		GL11.glTranslatef(0.0f, 0.0f, -3.0f);
		GL11.glRotatef(rotation[0], 0.0f, 1.0f, 0.0f);
		GL11.glRotatef(rotation[1], 1.0f, 0.0f, 0.0f);
		for (short i = 0; i<100; i++) {
			if (objects[i] != null)
				objects[i].render();
		}
		GL11.glPopMatrix();
	}
}
