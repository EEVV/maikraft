package lpr.voxels;

import org.lwjgl.opengl.GL11;
import lpr.renderer.Renderer;

public class VoxelObject {
	
	private float[][][][] voxels;
	private float[] transformation = {0.0f, 0.0f, 0.0f};
	
	public VoxelObject(String location) {
		voxels = VoxelFile.generateFF(location);
	}
	public VoxelObject(short x, short y, short z) {
		voxels = new float[x][y][z][5];
		fillNull();
	}
	public VoxelObject() {
		
	}
	
	public void fillNull() {
		for (short x = 0; x<voxels.length; x++) {
			for (short y = 0; y<voxels[0].length; y++) {
				for (short z = 0; z<voxels[0][0].length; z++) {
					for (byte i = 0; i<4; i++) {
						voxels[x][y][z][i] = 1.0f;
					}
				}
			}
		}
	}
	
	public void set(short x, short y, short z, float r, float g, float b, float a) {
		voxels[x][y][z][0] = r;
		voxels[x][y][z][1] = g;
		voxels[x][y][z][2] = b;
		voxels[x][y][z][3] = a;
	}
	public float getR(short x, short y, short z) {
		return voxels[x][y][z][0];
	}
	public float getG(short x, short y, short z) {
		return voxels[x][y][z][1];
	}
	public float getB(short x, short y, short z) {
		return voxels[x][y][z][2];
	}
	public float getA(short x, short y, short z) {
		return voxels[x][y][z][3];
	}
	
	public int getX() {
		return voxels.length;
	}
	public int getY() {
		return voxels[0].length;
	}
	public int getZ() {
		return voxels[0][0].length;
	}
	
	public void newVxl(float[][][][] newVoxels) {
		voxels = newVoxels;
	}
	public void transform(float x, float y, float z) {
		transformation[0] = x;
		transformation[1] = y;
		transformation[2] = z;
	}
	public void render() {
		
		float r = 1.0f;
		float g = 1.0f;
		float b = 1.0f;
		float a = 1.0f;
		//float s = 0.0f;
		
		for (short x = 0; x<voxels.length; x++) {
			for (short y = 0; y<voxels.length; y++) {
				for (short z = 0; z<voxels.length; z++) {
					r = voxels[x][y][z][0];
					g = voxels[x][y][z][1];
					b = voxels[x][y][z][2];
					a = voxels[x][y][z][3];
					float s = voxels[x][y][z][4];
					if (s == 1.0f)
						if (a > 0.0f)
							Renderer.renderCube(x / 2.0f+transformation[0], y / 2.0f+transformation[1], z / 2.0f+transformation[2], 0.25f, 0.25f, 0.25f, r, g, b, a);
					//System.out.println("r:"+r+" g:"+g+" b:"+b+" a:"+a);
				}
			}
		}
 	}
}
