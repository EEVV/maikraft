package lpr.editorCanvas;

import java.awt.Canvas;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.*;

import lpr.Input.InputHandler;
import lpr.voxels.ObjectHandler;
import lpr.voxels.VoxelFile;
import lpr.guiWindows.MainWindow;
import lpr.guiWindows.VoxelEditorWindow;

public class EditorCanvas implements Runnable {
	
	public void run() {
		try {
			Display.setDisplayMode(new DisplayMode(1280, 720));
			//Display.setParent(VoxelEditorWindow.canvas);
			Display.setResizable(true);
			Display.create();
		} catch (LWJGLException e) {
			e.printStackTrace();
			System.exit(0);
		}
		
		// Initializing opengl...
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(70, ((float) Display.getWidth() / (float) Display.getHeight()), 0.5f, 100.0f);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glClearColor(0.25f, 0.5f, 1.0f, 1.0f);
		
		init();
		
		while (!Display.isCloseRequested()) {
			
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
			GL11.glViewport(0, 0, Display.getWidth(), Display.getHeight());
			
			lpr.Input.InputHandler.update(Display.getWidth(), Display.getHeight());
			ObjectHandler.render();
			
			Display.update();
		}
		Display.destroy();
	}
	
	public void init() {
		ObjectHandler.init();
	}
}
