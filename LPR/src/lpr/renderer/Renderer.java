package lpr.renderer;

import org.lwjgl.opengl.GL11;

public class Renderer {
	public static void renderCube(float x, float y, float z, float xs, float ys, float zs, float r, float g, float b, float a) {
		GL11.glBegin(GL11.GL_QUADS);
		
			GL11.glColor4f(r, g, b, a);
			//Front
			GL11.glVertex3f(-xs+x, -ys+y, zs+z);
			GL11.glVertex3f(xs+x, -ys+y, zs+z);
			GL11.glVertex3f(xs+x, ys+y, zs+z);
			GL11.glVertex3f(-xs+x, ys+y, zs+z);
			//Back
			GL11.glVertex3f(-xs+x, -ys+y, -zs+z);
			GL11.glVertex3f(xs+x, -ys+y, -zs+z);
			GL11.glVertex3f(xs+x, ys+y, -zs+z);
			GL11.glVertex3f(-xs+x, ys+y, -zs+z);
			//Top
			GL11.glVertex3f(-xs+x, ys+y, zs+z);
			GL11.glVertex3f(xs+x, ys+y, zs+z);
			GL11.glVertex3f(xs+x, ys+y, -zs+z);
			GL11.glVertex3f(-xs+x, ys+y, -zs+z);
			//Bottom
			GL11.glVertex3f(-xs+x, -ys+y, zs+z);
			GL11.glVertex3f(xs+x, -ys+y, zs+z);
			GL11.glVertex3f(xs+x, -ys+y, -zs+z);
			GL11.glVertex3f(-xs+x, -ys+y, -zs+z);
			//Left
			GL11.glVertex3f(-xs+x, -ys+y, -zs+z);
			GL11.glVertex3f(-xs+x, -ys+y, zs+z);
			GL11.glVertex3f(-xs+x, ys+y, zs+z);
			GL11.glVertex3f(-xs+x, ys+y, -zs+z);
			//Right
			GL11.glVertex3f(xs+x, -ys+y, -zs+z);
			GL11.glVertex3f(xs+x, -ys+y, zs+z);
			GL11.glVertex3f(xs+x, ys+y, zs+z);
			GL11.glVertex3f(xs+x, ys+y, -zs+z);
			
		GL11.glEnd();
	}
}